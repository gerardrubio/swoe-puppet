#!/bin/bash
cd /var/www
rm -fr www.swoe.eu
git clone https://gerardrubio@bitbucket.org/gerardrubio/swoe.git www.swoe.eu

cd /var/www/www.swoe.eu
git submodule update --init --recursive

mkdir -p app/cache
chmod 0777 app/cache
cp app/config/config.php.prod app/config/config.php

ln -s /usr/local/bin/composer /usr/sbin/

composer install
composer dump-autoload -o
bower install --allow-root

cd /var/www/
rm -fr api.swoe.eu
git clone https://gerardrubio@bitbucket.org/gerardrubio/swoe-api.git api.swoe.eu
cd /var/www/api.swoe.eu
composer install
composer dump-autoload -o

cd /tmp
mongoimport --db swoe --collection cities --file import_cat.txt
mongo db.js
