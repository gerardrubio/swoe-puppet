use swoe;

var protocolBulk = db.getCollection('protocols').initializeOrderedBulkOp();
protocolBulk.insert({
	name: 'OBIS',
	default: true
});
protocolBulk.execute();

var protocol = db.getCollection('protocols').find()[0]._id.str;

var dimensionBulk = db.getCollection('dimensions').initializeOrderedBulkOp();
dimensionBulk.insert({
	name: 'Instantaneous Power',
	units: 'W',
	protocol: protocol,
	matcher: 'this.data.a == 1 && this.data.c == 1 && this.data.d == 7 && this.data.e == 0',
	status: 4,
	default: true
});
dimensionBulk.insert({
	name: 'Instantaneous Current',
	units: 'A',
	protocol: protocol,
	matcher: 'this.data.a == 1 && this.data.c == 1 && this.data.d == 11 && this.data.e == 0',
	status: 4,
	default: false
});
dimensionBulk.insert({
	name: 'Instantaneous Voltage',
	units: 'V',
	protocol: protocol,
	matcher: 'this.data.a == 1 && this.data.c == 1 && this.data.d == 12 && this.data.e == 0',
	status: 4,
	default: false
});
dimensionBulk.execute();

