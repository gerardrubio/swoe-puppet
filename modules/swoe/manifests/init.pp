class swoe () {
  # Edit local /etc/hosts files to resolve some hostnames used on your application.
  host { 'localhost':
  	ensure       => 'present', # no sobreescribe si existe
  	target       => '/etc/hosts',
  	ip           => '127.0.0.1',
  	host_aliases => ['www.swoe.eu', 'dev.swoe.eu', 'swoe-app', 'static.swoe.eu', 'api.swoe.eu', 'swoe.eu', 'mongo.swoe.eu', 'session.swoe.eu', 'cache.swoe.eu', 'swoe.eu']
  }

  #include epel

  # Instalación de paquetes batch, mediante una variable
  # instalamos las últimas versiones disponibles
  $misc_packages = ['vim-enhanced', 'telnet', 'zip', 'unzip', 'git', 'screen', 'libssh2', 'libssh2-devel', 'autoconf', 'automake', 'htop', 'ruby', 'zlib', 'zlib-devel', 'gcc-c++', 'patch', 'readline', 'readline-devel', 'libyaml-devel', 'libffi-devel', 'openssl-devel', 'make', 'bzip2', 'libtool', 'bison', 'curl', 'sqlite-devel', 'lynx', 'pcre-devel', 'php-devel', 'gd-devel', 'mongodb-org', 'tofrodos', 'libmcrypt', 'libtidy']#, 'iptables-services']

  exec {'yum-clean-all':
    command =>  'yum clean all',
    path    =>  '/usr/bin',
    user    =>  'root',
    before  =>  Package[$misc_packages],
  }

  package { $misc_packages:
  	ensure  => latest,
    require => Yumrepo['epel', 'remi', 'remi-php56', 'mongodb-org-3.0']
  }

  $file_i18n = '/etc/sysconfig/i18n'
  file { $file_i18n:
    ensure  => file,
    content => "LANG=en_GB.UTF-8"
  }

  $file_keyboard = '/etc/sysconfig/keyboard'
  file { $file_keyboard:
    ensure  => file,
    content => 'KEYTABLE="es"
      MODEL="pc105"
      LAYOUT="es"
      KEYBOARDTYPE="PC"'
  }

  $file_timezone = '/etc/localtime'
  file { $file_timezone:
    ensure  => link,
    target  => '/usr/share/zoneinfo/Europe/Madrid',
  }

  class { '::nginx':
    # Fix for "upstream sent too big header ..." errors
    fastcgi_buffers     => '8 8k',
    fastcgi_buffer_size => '8k',
    sendfile  => 'off',
  }

  service { 'php-fpm':
    enable  => true,
    ensure  => 'running',
  }

  file { '/etc/nginx/conf.d/www.swoe.eu.conf':
    ensure  => 'present',
    replace => true,
    source  => 'puppet:///modules/swoe/www.swoe.eu.conf',
    notify => Service['nginx', 'php-fpm']
  }

  file { '/etc/nginx/conf.d/api.swoe.eu.conf':
    ensure  => 'present',
    replace => true,
    source  => 'puppet:///modules/swoe/api.swoe.eu.conf',
    notify => Service['nginx', 'php-fpm']
  }

  $yum_repo = 'remi-php56'
  ::yum::managed_yumrepo { 'remi-php56':
    descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch - PHP 5.6',
    mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/php56/mirror',
    enabled        => 1,
    gpgcheck       => 1,
    gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
    gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',
    priority       => 1,
  }

  ::yum::managed_yumrepo { 'remi':
    descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch',
    mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/remi/mirror',
    enabled        => 1,
    gpgcheck       => 1,
    gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
    gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',
    priority       => 1,
  }

  ::yum::managed_yumrepo { 'mongodb-org-3.0':
    descr     => 'MongoDB Repository',
    baseurl   => 'https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.0/x86_64/',
    enabled   => 1,
    gpgcheck  => 0
  }

  class { 'php':
    version => 'latest',
    service => 'nginx',
    require => Yumrepo[$yum_repo],
    before => File['copy_php.ini']
  }

  php::module { [ 'devel', 'pear', 'tidy', 'soap', 'ldap', 'mcrypt', 'pdo', 'gd', 'mbstring', 'xml', 'phalcon2', 'fpm', 'pecl-mongo', 'pecl-mongodb', 'intl', 'pecl-xdebug', 'pecl-memcached', 'pecl-memcache' ]: }

  class { 'iptables':;
  }

  class { 'selinux':
    mode  => 'disabled',
    notify => Reboot['selinux_reboot']
  }

  reboot { 'selinux_reboot':
    apply => 'finished',
    when  => 'refreshed'
  }

#  file { 'copy_php-ini_script':
#    ensure  => 'file',
#    source  => 'puppet:///modules/swoe/php-ini.sh',
#    path    => '/tmp/php-ini.sh',
#    owner   => 'root',
#    group   => 'root',
#    mode    => '0744',
#    notify  => Exec['run_php-ini_script'],
#  }

  file { 'copy_swoe-generate-readings_script':
    ensure  => 'file',
    source  => 'puppet:///modules/swoe/swoe-update.sh',
    path    => '/tmp/swoe-update.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
  }

  exec { 'run_php-ini_script':
    command => '/bin/bash /tmp/php-ini.sh',
    refreshonly => true
  }

  class { 'composer':
    auto_update => true,
    require     => Class['php'],
  }

  #cron::job { 'generate-readings':
  #  command => '/tmp/swoe-update.sh',
  #}

  class {'newrelic::server::linux':
    newrelic_license_key => 'd3370922cc9bfd99171b8cd7d31eed21b3937ed4',
  }

  class {'newrelic::agent::php':
    newrelic_license_key  => 'd3370922cc9bfd99171b8cd7d31eed21b3937ed4',
    newrelic_ini_appname  => 'SWoE',
    newrelic_php_conf_dir => ['/etc/php.d','/etc/php-fpm.d'],
    notify => Service['php-fpm','nginx']
  }

  file { 'copy_phalcon-devtools_script':
    ensure  => 'file',
    source  => 'puppet:///modules/swoe/phalcon-devtools.sh',
    path    => '/tmp/phalcon-devtools.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    require => Service['php-fpm'],
    before  => Exec['run_phalcon-devtools_script']
  }

  exec { 'run_phalcon-devtools_script':
    command => '/bin/bash /tmp/phalcon-devtools.sh',
    refreshonly => true,
  }

  file { 'copy_php.ini':
    ensure  => 'file',
    source  => 'puppet:///modules/swoe/php.ini',
    path    => '/tmp/php.ini',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    before  => File['move_php_ini.sh']
  }

  file { 'move_php_ini.sh':
    ensure  => 'file',
    source  => 'puppet:///modules/swoe/move_php_ini.sh',
    path    => '/tmp/move_php_ini.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    before  => Exec['exec_move_php_ini']
  }

  exec { 'exec_move_php_ini':
    command     => '/bin/bash /tmp/move_php_ini.sh',
    refreshonly => true
  }

  file { 'copy_php-fpm.conf':
    ensure  => 'file',
    source  => 'puppet:///modules/swoe/php-fpm.conf',
    path    => '/etc/php-fpm.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    notify => Service['php-fpm', 'nginx'],
  }

  file { 'copy_php_www.conf':
    ensure  => 'file',
    source  => 'puppet:///modules/swoe/www.conf',
    path    => '/etc/php-fpm.d/www.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    notify => Service['php-fpm', 'nginx'],
  }

  class { '::nodejs':
    manage_package_repo       => false,
    nodejs_dev_package_ensure => 'present',
    npm_package_ensure        => 'present',
    require                   => Package[$misc_packages]
  }

  package { 'bower':
    ensure   => 'present',
    provider => 'npm',
    require  => Class['nodejs']
  }

  class { 'memcached':
    max_memory  => '12%',
    tcp_port    => 11211
  }

  file { 'install_swoe.sh':
    ensure  => 'file',
    source  => 'puppet:///modules/swoe/install_swoe.sh',
    path    => '/tmp/install_swoe.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    before  => Exec['exec_install_swoe.sh']
  }

  exec { 'exec_install_swoe.sh':
    command     => '/bin/bash /tmp/install_swoe.sh',
    refreshonly => true
  }
}
